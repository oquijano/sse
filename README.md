# Server Sent Events Library for Racket
----

This is an implementation of server sent events (SSE) for
racket. An explanations of SSE's can be found in this [MDN web
doc](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events).

The current version (1.0) is not backwards compatible with the
previous one. You can still access the old functions by doing
`(require SSE/legacy)`. Do not import both `SSE/legacy` and `SSE`, you
should only import one of them.


## Installation

In order to install this package clone the repository, enter the
cloned folder and run the following command

```bash
raco pkg install SSE
```

## Documentation links

+ Current version (1.0) : [https://oquijano.net/SSE/v1.0/](https://oquijano.net/SSE/v1.0/)
+ Old version (0.1) : [https://oquijano.net/SSE/v0.1/](https://oquijano.net/SSE/v0.1/)
