#lang scribble/manual

@require[@for-label[SSE
                    racket/base
		    web-server/http
		    web-server/web-server
		    web-server/dispatchers/dispatch]]

@title{SSE}
@author{Oscar Alberto Quijano Xacur}

@defmodule[SSE]

@section{Example of use}

Let us start by defining a function that serves a website with a text area that
is filled with messages coming from server sent events on port 8122.

@codeblock|{
#lang racket

(require web-server/http/xexpr
         web-server/web-server
	 web-server/servlet-dispatch
	 SSE)

;; Page that connects to the server-sent events
(define (start-page req)
    (response/xexpr
	`(html
	  (head
	  (meta ([http-equiv "content-type"] [content "text/html; charset=utf-8"] ))
	  (meta ([name "viewport"] [content "width=device-width"] ))
	  (title "Events test"))

	  (body
	  (h1 "Welcome to the Events test")
	  (textarea ([readonly ""]))
	  (script
	  "
	  var evtSource = new EventSource(\"//localhost:8122\");
	  var textArea = document.getElementsByTagName(\"textarea\")[0];

	  evtSource.onmessage = function(e){
	      textArea.value =  e.data + \"\\n\" + textArea.value ;
	      console.log(e.data);
	  }
 	  ")))))
}|

Now we define the server sent events, serve them on port 8122 and run the
servlet for the function above on port 8080.

@codeblock|{
(define a-sse (make-sse))
(sse-serve a-sse #:port 8122)
(serve #:dispatch (dispatch/servlet start-page) #:port 8080)
}|

Note that we are not using the function @racket[serve/servlet] for serving the
website. @racket[serve] does not block the racket REPL. Now open a browser and
visit @url{http://localhost:8080}. You are going to
see a welcome message and an empty textarea. In order to add some text
send a data only event as follows:

@codeblock{(sse-send-event a-sse #:data "Hello World!")}

At this point the text "Hello World!" appears in the text area.

Now open a new tab or window of your browser and go again to
@racket[localhost:8080]. Note that the message "Hello World!" does not
appear in this window. This is because we used @racket[#:id] with
value @racket[#f] (it is the default value). Let us now send one with
@racket[#:id] equal to @racket[#t].

@codeblock{(sse-send-event a-sse #:data "Hello World! (again)" #:id #t)}

At this point the two windows that you have open will show the message
"Hello World! (again)". This would be the case even with @racket[#:id]
equal to @racket[#f]. What makes the difference is that if you open
now a new tab and connect to @racket[localhost:8080], that window will
also show this message.


@section{HTTP responses}

Server Sent Events (SSE) connections start with an http request for
connection. The
@hyperlink["https://html.spec.whatwg.org/multipage/server-sent-events.html"
"SSE standard"] allows 4 possible responses. The library has a way to represent
each of them as shown in the following table.

@tabular[#:style 'boxed
         #:column-properties '(center center)
         #:sep @hspace[1]
	 (list (list @bold{HTTP Status Code} @bold{Library Representation} )
	       (list "200 Accept" "'accept")
	       (list "204 No Content" "'no-content")
	       (list "301 Moved Permanently" "'(moved-permanently \"http://new-location.com\")")
	       (list "307 Temporary Redirect" "'(temporary-redirect \"http://new-location.com\")")
	 )
]

For the two hundred codes, the representation is simply a symbol. For the three
hundred codes, it is a list with a symbol and string with the url of the
redirection. A predicate to check if something is a proper representation of
one of the http responses above.

@defproc[(sse-http-response? [x any/c]) boolean?]

@section{Provided functions}

To see if the value of a variable is a sse, the following predicate can be
used.
@defproc[(sse? [x any/c]) boolean?]{}

Racket servlets use functions that take an HTTP request and return an HTTP
response. Similarly, the function @racket[make-sse] takes a parameter called
the initial request servlet, which takes a request and returns one of the four
http responses allowed for SSE. If the response is @racket['accept], a
connection for the event stream is established.

@defproc[(make-sse [initial-request-servlet (-> request? sse-http-response?) (lambda (req) 'accept) ]) sse?]

Once a sse variable has been created it can be served on a specific port with
the @racket[sse-serve] function and messages can be sent using
@racket[sse-send-event].

@defproc[(sse-serve [a-sse sse?] [#:port port listen-port-number?] ) void?]

@defproc[(sse-send-event
	  [a-sse sse?]
	  [#:data data (or/c string? null?) empty]
	  [#:event event (or/c string? null?) empty]
	  [#:id id boolean? #f]
	  [#:retry retry (or/c positive-integer? null?) empty ])
	  void?]{
	  
	  If @racket[id] is @racket[#t] the message is send and it is
	  added to the @racket[message-hash] of @racket[a-sse]. The
	  actual @racket[id] value sent is always a positive integer
	  starting from one. It is automatically managed by the
	  library. The messages added to the @racket[message-hash] are
	  sent to new connections. When the header
	  @racket[Last-Event-ID] is sent in a request only the
	  messages with an @racket[id] greater than that value are sent.
	  
}	


@defproc[(sse-dispatcher [x sse?]) dispatcher/c]{Returns the
@hyperlink["https://docs.racket-lang.org/web-server-internal/dispatchers.html"]{dispatcher} 
associated with a sse.}

@defproc[(sse-restart-messages [a-sse sse?]) void?]{Erases the history of sent messages.}
